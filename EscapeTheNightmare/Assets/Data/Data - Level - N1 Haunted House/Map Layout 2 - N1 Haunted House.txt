<layout>
	<name>two</name>
	<exitType>East</exitType>
	<rooms>
		<room>
			<x>0</x>
			<y>0</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>1</exitPoint>
		</room>
		<room>
			<x>0</x>
			<y>2</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>0</x>
			<y>3</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>1</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>1</x>
			<y>0</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>1</x>
			<y>1</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>1</x>
			<y>2</y>
			<enemySpawn>1</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>1</x>
			<y>3</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>2</x>
			<y>1</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>2</x>
			<y>2</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>3</x>
			<y>2</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
		<room>
			<x>3</x>
			<y>3</y>
			<enemySpawn>0</enemySpawn>
			<playerSpawn>0</playerSpawn>
			<exitPoint>0</exitPoint>
		</room>
	</rooms>
</layout>