﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Fade to black screen. Fade up first title. Fade up second title. Fade to black. Load next scene.
/// </summary>
public class EscapeScreen : MonoBehaviour
{
    public Image background;
    public Text firstTitle;
    public Text secondTitle;

    private const float timeBetweenTexts = 1.5f;
    private const float timeInterval = 0.1f;

    public event Action Completed;

    // Start is called before the first frame update
    void Start()
    {
        HideGraphics();
    }

    private void HideGraphics()
    {
        Vector4 colorVec = background.color;
        colorVec.w = 0;
        background.color = colorVec;

        colorVec = firstTitle.color;
        colorVec.w = 0;
        firstTitle.color = colorVec;

        colorVec = secondTitle.color;
        colorVec.w = 0;
        secondTitle.color = colorVec;
    }
    
    public void Show()
    {
        InvokeRepeating("FadeUpBackground", 0, timeInterval);
    }

    /// <summary>
    /// Fade up the background image from transparent to opaque. Then start next fade.
    /// </summary>
    private void FadeUpBackground()
    {
        // get the current color
        Vector4 colorVec = background.color;
        // make the color more opaque
        colorVec.w += 0.05f;
        // check if color is completely opaque
        if (colorVec.w >= 1)
        {
            // start next fade
            InvokeRepeating("FadeUpFirstTitle", 0.5f, timeInterval);
            // stop this fade
            CancelInvoke("FadeUpBackground");
        }
        // set new color
        background.color = colorVec;
    }

    /// <summary>
    /// Fade up the first title from transparent to opaque. Then start next fade.
    /// </summary>
    private void FadeUpFirstTitle()
    {
        // get the current color
        Vector4 colorVec = firstTitle.color;
        // make the color more opaque
        colorVec.w += 0.05f;
        // check if color is completely opaque
        if (colorVec.w >= 1)
        {
            // start next fade
            InvokeRepeating("FadeUpSecondTitle", timeBetweenTexts, timeInterval);
            // stop this fade
            CancelInvoke("FadeUpFirstTitle");
        }
        // set new color
        firstTitle.color = colorVec;
    }

    /// <summary>
    /// Fade up second title from transparent to opaque. Then start next fade.
    /// </summary>
    private void FadeUpSecondTitle()
    {
        // get the current color
        Vector4 colorVec = secondTitle.color;
        // make the color more opaque
        colorVec.w += 0.04f;
        // check if color is completely opaque
        if (colorVec.w >= 1)
        {
            // start next fade
            InvokeRepeating("FadeDownAllText", 2, timeInterval);
            // stop this fade
            CancelInvoke("FadeUpSecondTitle");
        }
        // set new color
        secondTitle.color = colorVec;
    }

    /// <summary>
    /// Fade all the text to transparent
    /// </summary>
    private void FadeDownAllText()
    {
        // get the current color
        Vector4 colorVec = firstTitle.color;
        Vector4 colorVec2 = secondTitle.color;
        // make the color more transparent
        colorVec.w -= 0.08f;
        colorVec2.w -= 0.08f;
        // check if color is completely transparent
        if (colorVec.w <= 0)
        {
            Completed?.Invoke();
            // stop this fade
            CancelInvoke("FadeDownAllText");
        }
        // set new color
        firstTitle.color = colorVec;
        secondTitle.color = colorVec2;
    }

}
