﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.IO;
using System;

public class MapLayoutParser : MonoBehaviour
{
    // each text file should hold 1 map layout
    public MapSettings ParseMapLayout(TextAsset dataFile)
    {
        MapSettings layout = new MapSettings();
        layout.roomIndexes = new List<Vector2Int>();

        XmlDocument doc = new XmlDocument();
        doc.LoadXml(dataFile.text);

        XmlNodeList rooms = doc.GetElementsByTagName("room");

        foreach (XmlNode item in rooms)
        {
            var index = new Vector2Int(int.Parse(item.ChildNodes[0].InnerText), int.Parse(item.ChildNodes[1].InnerText));
            layout.roomIndexes.Add(index);
            if (item.ChildNodes[2].InnerText.CompareTo("1") == 0) layout.enemySpawnRoomIndex = index;
            if (item.ChildNodes[3].InnerText.CompareTo("1") == 0) layout.playerSpawnRoomIndex = index;
            if (item.ChildNodes[4].InnerText.CompareTo("1") == 0) layout.exitPointRoomIndex = index;
        }
        layout.exitPointDirection =
            doc.GetElementsByTagName("exitType")[0].InnerText == "North" ? Direction.North :
            (doc.GetElementsByTagName("exitType")[0].InnerText == "South" ? Direction.South :
            (doc.GetElementsByTagName("exitType")[0].InnerText == "East" ? Direction.East : Direction.West));        

        return layout;
    }
}
