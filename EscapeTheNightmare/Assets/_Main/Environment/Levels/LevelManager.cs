﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    private LevelSettings LevelSettings => GameManager.Instance.LevelManifest.Levels[GameManager.Instance.CurrentGameState.LastLevelWon];

    private MapSettings currentMapSettings;

    private Room[,] map;

    private float startTime;

    private PlaythroughData playthroughData;
    
    private void Start()
    {
        playthroughData = new PlaythroughData() { Level = LevelSettings.levelIndex };

        //FindObjectOfType<Enemy>().CollidedWithPlayer += () => playthroughData.EnemyEncounters++;
        // GoodPointsEarned, BadPointsEarned, SecretFound, Clue1Found, ...

        // Something.DoneLoading += () => startTime = Time.time;
        
        startTime = Time.time;

        GenerateMap();
    }

    private void GenerateMap()
    {
        if (map != null)
        {
            foreach (var item in map)
            {
                Destroy(item);
            }
        }

        int rand = UnityEngine.Random.Range(0, LevelSettings.mapLayouts.Count);
        map = gameObject.GetComponent<MapGenerator>().GenerateMap(currentMapSettings = LevelSettings.mapLayouts[rand]);
    }

    public void EndLevel(float endTime, bool win, DeathType death)
    {
        foreach (CharacterMovement item in FindObjectsOfType<CharacterMovement>())
        {
            item.Stopped = true;
        }

        //! save data from this level run
        playthroughData.Time = endTime - startTime;
        playthroughData.Win = win;
        playthroughData.DeathType = death;

        GameManager.Instance.CurrentGameState.AddPlaythroughData(playthroughData);
        SaveSystem.SaveProgressData(GameManager.Instance.CurrentGameState);

        // make the player character walk through the door
        Vector2 playerDirection = Vector2.zero;
        float moveSpeed = 10f;
        switch (currentMapSettings.exitPointDirection)
        {
            case Direction.North:
                playerDirection.y = moveSpeed;
                break;
            case Direction.South:
                playerDirection.y = -moveSpeed;
                break;
            case Direction.East:
                playerDirection.x = moveSpeed;
                break;
            case Direction.West:
                playerDirection.x = -moveSpeed;
                break;
            default:
                break;
        }
        FindObjectOfType<PlayerMovement>().GetComponent<Rigidbody2D>().AddForce(playerDirection);

        // show the end of level screen, then load apartment scene (plays cutscene first)
        FindObjectOfType<EscapeScreen>().Completed += () => SceneManager.LoadScene("Apartment");
        FindObjectOfType<EscapeScreen>().Show();        
    }
}
