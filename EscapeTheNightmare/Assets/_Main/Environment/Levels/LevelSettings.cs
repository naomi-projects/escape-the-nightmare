﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "LevelSettings")]
public class LevelSettings : ScriptableObject
{
    public int levelIndex;
    public string levelName;
    public string sceneName;
    public List<MapSettings> mapLayouts;
}
