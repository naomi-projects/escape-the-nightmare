﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public BoxCollider2D doorTrigger;
    public BoxCollider2D block;

    public bool locked;
    public bool Locked {
        get
        {
            return locked;
        }
        set
        {
            locked = value;
            doorTrigger.isTrigger = !locked;
        }
    }

    public bool isExit;
    public bool IsExit { get { return isExit; }
        set {
            if (value)
            {
                block.enabled = false;
                doorTrigger.isTrigger = true;
                Locked = false;
            }
            isExit = value;
        }
    }

    public Vector2 PlayerDestination { get; set; }
    public Vector2 CameraDestination { get; set; }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Locked) return;

        if (collision.tag == "Player")
        {
            if (isExit)
            {
                // player exit level
                Debug.Log("Player reached the exit.");
                FindObjectOfType<LevelManager>().EndLevel(Time.time, true, DeathType.NoDeath);
                return;
            }

            // move player to next room
            collision.gameObject.transform.position = PlayerDestination;

            // move camera to next room
            Vector3 camPos = Camera.main.transform.position;
            camPos.x = CameraDestination.x;
            camPos.y = CameraDestination.y;
            Camera.main.transform.position = camPos;

        }
    }
}
