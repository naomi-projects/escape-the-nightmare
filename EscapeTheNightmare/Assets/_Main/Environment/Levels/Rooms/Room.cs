﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public Door[] doors;
    public float eastOffset = 1f;
    public float westOffset = -1f;
    public float northOffset = 0.33f;
    public float southOffset = -0.5f;
    
    public void SetUpDoor(Direction dir, bool locked = true, Vector2? destinationRoom = null)
    {
        doors[(int)dir].Locked = locked;
        if (locked) return;

        doors[(int)dir].CameraDestination = (Vector2)destinationRoom;
        Vector2 offset = Vector2.zero;
        switch (dir)
        {
            case Direction.North:
                offset.y = southOffset;
                break;
            case Direction.South:
                offset.y = northOffset;
                break;
            case Direction.East:
                offset.x = westOffset;
                break;
            case Direction.West:
                offset.x = eastOffset;
                break;
            default:
                break;
        }

        doors[(int)dir].PlayerDestination = (Vector2)destinationRoom + offset;
    }
}
