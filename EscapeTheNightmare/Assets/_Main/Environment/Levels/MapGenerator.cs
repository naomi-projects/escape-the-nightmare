﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Room[,] GenerateMap(MapSettings mapSettings)
    {    
        /// Generate rooms
        Room[,] map = new Room[mapSettings.maxMapDimensions, mapSettings.maxMapDimensions];

        foreach (Vector2Int index in mapSettings.roomIndexes)
        {
            Vector3 position = new Vector3(index.x * mapSettings.roomOffset, index.y * mapSettings.roomOffset, 0);
            int randRoom = UnityEngine.Random.Range(0, mapSettings.roomPrefabs.Count);
            map[index.x, index.y] = Instantiate(mapSettings.roomPrefabs[randRoom], position, Quaternion.identity).GetComponent<Room>();
        }
        
        /// Lock/unlock doors
        for (int i = 0; i < mapSettings.maxMapDimensions; i++)
        {
            for (int j = 0; j < mapSettings.maxMapDimensions; j++)
            {
                if (map[i, j] != null)
                {

                    map[i, j].SetUpDoor(Direction.East);
                    map[i, j].SetUpDoor(Direction.West);
                    map[i, j].SetUpDoor(Direction.North);
                    map[i, j].SetUpDoor(Direction.South);

                    // Right door
                    if (i < mapSettings.maxMapDimensions)
                    {
                        if (map[i + 1, j] != null)
                        {
                            // Unlock
                            map[i, j].SetUpDoor(Direction.East, false, map[i + 1, j].transform.position);
                        }         
                    }
                    // Left door
                    if (i > 0)
                    {
                        if (map[i - 1, j] != null)
                        {
                            // Unlock
                            map[i, j].SetUpDoor(Direction.West, false, map[i - 1, j].transform.position);
                        }
                    }
                    // Bottom door
                    if (j > 0)
                    {
                        if (map[i, j - 1] != null)
                        {
                            // Unlock
                            map[i, j].SetUpDoor(Direction.South, false, map[i, j - 1].transform.position);
                        }
                    }
                    // Top door
                    if (j < mapSettings.maxMapDimensions)
                    {
                        if (map[i, j + 1] != null)
                        {
                            // Unlock
                            map[i, j].SetUpDoor(Direction.North, false, map[i, j + 1].transform.position);
                        }
                    }
                }
            }
        }

        /// Set the exit point
        Vector2Int exit = mapSettings.exitPointRoomIndex;
        Direction exitDirection = mapSettings.exitPointDirection;
        map[exit.x, exit.y].doors[(int)exitDirection].IsExit = true;

        return map;
    }    
}