﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MapSettings")]
public class MapSettings : ScriptableObject
{
    public List<Vector2Int> roomIndexes;
    public Vector2Int playerSpawnRoomIndex;
    public Vector2Int enemySpawnRoomIndex;
    public Vector2Int exitPointRoomIndex;
    public Direction exitPointDirection;

    public Vector2 playerSpawnPointOffset;
    public Vector2 enemySpawnPointOffset;

    public List<GameObject> roomPrefabs;

    public int maxMapDimensions = 12;
    public float roomOffset = 10;
}

public enum Direction { North, South, East, West }