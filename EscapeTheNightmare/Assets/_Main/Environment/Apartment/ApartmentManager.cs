﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ApartmentManager : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        // press enter to load the level
        if (Input.GetKeyDown(KeyCode.Return))
        {
            LoadLevel();
        }
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("Level");

        Debug.Log("Loading level: " + GameManager.Instance.LevelManifest.Levels[GameManager.Instance.CurrentGameState.LastLevelWon].levelName);
    }
}
