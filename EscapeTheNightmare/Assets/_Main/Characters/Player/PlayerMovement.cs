﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : CharacterMovement
{
    private const float walkSpeed = 0.01f;
    private const float sprintSpeed = 0.04f;

    public bool Sprinting => Input.GetKey(KeyCode.LeftShift) ? true : false;
    private float CurrentSpeed => Sprinting ? sprintSpeed : walkSpeed;
        
    void Update()
    {
        if (Stopped) return;

        Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        direction.y = direction.y > 0 ? 1 : (direction.y < 0 ? -1 : direction.y);
        direction.x = direction.x > 0 ? 1 : (direction.x < 0 ? -1 : direction.x);

        rb.MovePosition(rb.position + (direction.normalized * CurrentSpeed));
    }
}
