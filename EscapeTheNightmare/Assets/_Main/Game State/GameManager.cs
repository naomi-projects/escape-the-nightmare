﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        CurrentGameState = SaveSystem.LoadProgressData();
        if (CurrentGameState == null) CurrentGameState = new GameState();        
    }

    public GameState CurrentGameState { get; private set; }

    public LevelManifest LevelManifest;
}

[CreateAssetMenu(menuName = "LevelManifest")]
public class LevelManifest : ScriptableObject
{
    public List<LevelSettings> Levels;
}


