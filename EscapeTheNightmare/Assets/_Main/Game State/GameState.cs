﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameState
{
    public const int MAX_LEVELS = 7;

    // Level Data
    public int LastLevelWon { get; private set; }

    [SerializeField]
    private List<PlaythroughData>[] playthroughs;

    public GameState()
    {
        playthroughs = new List<PlaythroughData>[MAX_LEVELS + 1];
        for (int i = 1; i < MAX_LEVELS + 1; i++)
        {
            playthroughs[i] = new List<PlaythroughData>();
        }
    }

    /// Updating the Game State data
    public void AddPlaythroughData(PlaythroughData data)
    {
        if (data.Win && LastLevelWon < data.Level) LastLevelWon = data.Level;

        playthroughs[data.Level].Add(data);
    }

    /// Accessing the Game State data

    public int NumberOfPlaythroughs(int level, bool onlyWins = false)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        if (!onlyWins) return playthroughs[level].Count;

        int num = 0;

        foreach (PlaythroughData item in playthroughs[level])
        {
            if (item.Win) num++;
        }

        return num;
    }

    public int TotalPlaythroughs(bool onlyWins = false)
    {
        int num = 0;

        for (int i = 1; i <= MAX_LEVELS; i++)
        {
            foreach (PlaythroughData item in playthroughs[i])
            {
                if (!onlyWins || item.Win) num++;
            }
        }

        return num;
    }

    public float AverageTime(int level, bool onlyWins = false)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        float totalTime = 0;
        int num = 0;

        foreach (PlaythroughData item in playthroughs[level])
        {
            if (!onlyWins || item.Win)
            {
                totalTime += item.Time;
                num++;
            }
        }

        if (num == 0) return -1;

        return totalTime / (float)num;
    }

    public float TotalTime(int level, bool onlyWins = false)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        float totalTime = 0;

        foreach (PlaythroughData item in playthroughs[level])
        {
            if (!onlyWins || item.Win)
            {
                totalTime += item.Time;
            }
        }        

        return totalTime;
    }

    public float ShortestTime(int level, bool onlyWins = false)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        float fastest = -1;

        foreach (PlaythroughData item in playthroughs[level])
        {
            if (!onlyWins || item.Win)
            {
                if (item.Time < fastest || fastest < 0) fastest = item.Time;
            }
        }

        return fastest;
    }

    public float LongestTime(int level, bool onlyWins = false)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        float longest = -1;

        foreach (PlaythroughData item in playthroughs[level])
        {
            if (!onlyWins || item.Win)
            {
                if (item.Time > longest) longest = item.Time;
            }
        }

        return longest;
    }

    public int SecretFound(int level)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        foreach (PlaythroughData item in playthroughs[level])
        {
            if (item.SecretFound) return 1;
        }

        return 0;
    }

    public int ClueFound(int level, int clue)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        if (clue == 1)
        {
            foreach (PlaythroughData item in playthroughs[level])
            {
                if (item.Clue1Found) return 1;
            }
        }
        else if (clue == 2)
        {
            foreach (PlaythroughData item in playthroughs[level])
            {
                if (item.Clue2Found) return 1;
            }
        }
        else if (clue == 3)
        {
            foreach (PlaythroughData item in playthroughs[level])
            {
                if (item.Clue3Found) return 1;
            }
        }
        else
        {
            return -1;
        }

        return 0;
    }

    public int ItemFound(int level, int item)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        if (item == 1)
        {
            foreach (PlaythroughData data in playthroughs[level])
            {
                if (data.Item1Found) return 1;
            }
        }
        else if (item == 2)
        {
            foreach (PlaythroughData data in playthroughs[level])
            {
                if (data.Item2Found) return 1;
            }
        }
        else
        {
            return -1;
        }

        return 0;
    }

    public int EnemyEncounters(int level)
    {
        if (level < 1 || level > MAX_LEVELS) return -1;

        int num = 0;

        foreach (PlaythroughData item in playthroughs[level])
        {
            num += item.EnemyEncounters;
        }

        return num;
    }
    
}

public enum DeathType { _, Fear, Enemy, Stress, NoDeath }

[System.Serializable]
public class PlaythroughData
{
    public int Level { get; set; }  
    public float Time { get; set; }
    public bool Win { get; set; }
    public DeathType DeathType { get; set; }

    public int EnemyEncounters { get; set; }
    public int GoodPointsEarned { get; set; }
    public int BadPointsEarned { get; set; }

    public bool SecretFound { get;  set; }
    public bool Clue1Found { get;  set; }
    public bool Clue2Found { get;  set; }
    public bool Clue3Found { get;  set; }
    public bool Item1Found { get;  set; }
    public bool Item2Found { get;  set; }    
}


