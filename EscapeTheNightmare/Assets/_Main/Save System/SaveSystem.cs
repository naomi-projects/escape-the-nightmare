﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveProgressData(GameState gameState)
    {
        Debug.Log("Saving progress...");
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/ETN.save";
        FileStream stream = new FileStream(path, FileMode.Create);
        
        formatter.Serialize(stream, gameState);
        stream.Close();
    }

    public static GameState LoadProgressData()
    {
        Debug.Log("Loading progress...");
        string path = Application.persistentDataPath + "/ETN.save";
        if (File.Exists(path))
        {
            Debug.Log("Save file found in " + path);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameState gameState = formatter.Deserialize(stream) as GameState;

            stream.Close();

            return gameState;
        }
        else
        {
            Debug.LogWarning("Save file not found in " + path);
            return null;
        }
    }
}
